from registro.models import Persona, Perro
from rest_framework import serializers


class PersonaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Persona
        fields = ('username','contrasenia','recontrasenia','correo','run','nombre','fnacimiento','ntelefono','region','comuna','casa')

class PerroSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Perro
        fields = ('nombrep','raza','descripcion','estado','fotito')
