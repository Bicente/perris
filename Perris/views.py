from django.contrib.auth.models import Persona, Perro
from rest_framework import serializers, viewsets
from Perris.serializers import PerroSerializer, PersonaSerializer
 
class PersonaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Persona.objects.all().order_by('nombre')
    serializer_class = PersonaSerializer
 
class PerroViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Perro.objects.all()
    serializer_class = PerroSerializer
