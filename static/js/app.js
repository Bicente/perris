
/*Arreglo de regiones/comunas */
$(function () {

    regiones.regiones.forEach(region => {
        $("#region").append('<option value="' + region.region + '">' + region.region + '</option>')
    });

})

$("#region").change(function () {
    $("#comuna").html("")
    var region = $(this).val()
    var comunas = regiones.regiones.find(item => item.region == region);
    $("#comuna").append('<option hidden >Seleccione</option>')
    comunas.comunas.sort().forEach(comuna => {
        $("#comuna").append('<option value="' + comuna + '">' + comuna + '</option>')
    });
})
var regiones = {
    "regiones": [{
        "region": "Arica y Parinacota",
        "comunas": ["Arica", "Camarones", "Putre", "General Lagos"]
    },
    {
        "region": "Tarapacá",
        "comunas": ["Iquique", "Alto Hospicio", "Pozo Almonte", "Camiña", "Colchane", "Huara", "Pica"]
    },
    {
        "region": "Antofagasta",
        "comunas": ["Antofagasta", "Mejillones", "Sierra Gorda", "Taltal", "Calama", "Ollagüe", "San Pedro de Atacama", "Tocopilla", "María Elena"]
    },
    {
        "region": "Atacama",
        "comunas": ["Copiapó", "Caldera", "Tierra Amarilla", "Chañaral", "Diego de Almagro", "Vallenar", "Alto del Carmen", "Freirina", "Huasco"]
    },
    {
        "region": "Coquimbo",
        "comunas": ["La Serena", "Coquimbo", "Andacollo", "La Higuera", "Paiguano", "Vicuña", "Illapel", "Canela", "Los Vilos", "Salamanca", "Ovalle", "Combarbalá", "Monte Patria", "Punitaqui", "Río Hurtado"]
    },
    {
        "region": "Valparaíso",
        "comunas": ["Valparaíso", "Casablanca", "Concón", "Juan Fernández", "Puchuncaví", "Quintero", "Viña del Mar", "Isla de Pascua", "Los Andes", "Calle Larga", "Rinconada", "San Esteban", "La Ligua", "Cabildo", "Papudo", "Petorca", "Zapallar", "Quillota", "Calera", "Hijuelas", "La Cruz", "Nogales", "San Antonio", "Algarrobo", "Cartagena", "El Quisco", "El Tabo", "Santo Domingo", "San Felipe", "Catemu", "Llaillay", "Panquehue", "Putaendo", "Santa María", "Quilpué", "Limache", "Olmué", "Villa Alemana"]
    },
    {
        "region": "Región del Libertador Gral. Bernardo O’Higgins",
        "comunas": ["Rancagua", "Codegua", "Coinco", "Coltauco", "Doñihue", "Graneros", "Las Cabras", "Machalí", "Malloa", "Mostazal", "Olivar", "Peumo", "Pichidegua", "Quinta de Tilcoco", "Rengo", "Requínoa", "San Vicente", "Pichilemu", "La Estrella", "Litueche", "Marchihue", "Navidad", "Paredones", "San Fernando", "Chépica", "Chimbarongo", "Lolol", "Nancagua", "Palmilla", "Peralillo", "Placilla", "Pumanque", "Santa Cruz"]
    },
    {
        "region": "Región del Maule",
        "comunas": ["Talca", "Constitución", "Curepto", "Empedrado", "Maule", "Pelarco", "Pencahue", "Río Claro", "San Clemente", "San Rafael", "Cauquenes", "Chanco", "Pelluhue", "Curicó", "Hualañé", "Licantén", "Molina", "Rauco", "Romeral", "Sagrada Familia", "Teno", "Vichuquén", "Linares", "Colbún", "Longaví", "Parral", "Retiro", "San Javier", "Villa Alegre", "Yerbas Buenas"]
    },
    {
        "region": "Región de Ñuble",
        "comunas": ["Cobquecura", "Coelemu", "Ninhue", "Portezuelo", "Quirihue", "Ránquil", "Treguaco", "Bulnes", "Chillán Viejo", "Chillán", "El Carmen", "Pemuco", "Pinto", "Quillón", "San Ignacio", "Yungay", "Coihueco", "Ñiquén", "San Carlos", "San Fabián", "San Nicolás"]
    },
    {
        "region": "Región del Biobío",
        "comunas": ["Concepción", "Coronel", "Chiguayante", "Florida", "Hualqui", "Lota", "Penco", "San Pedro de la Paz", "Santa Juana", "Talcahuano", "Tomé", "Hualpén", "Lebu", "Arauco", "Cañete", "Contulmo", "Curanilahue", "Los Álamos", "Tirúa", "Los Ángeles", "Antuco", "Cabrero", "Laja", "Mulchén", "Nacimiento", "Negrete", "Quilaco", "Quilleco", "San Rosendo", "Santa Bárbara", "Tucapel", "Yumbel", "Alto Biobío"]
    },
    {
        "region": "Región de la Araucanía",
        "comunas": ["Temuco", "Carahue", "Cunco", "Curarrehue", "Freire", "Galvarino", "Gorbea", "Lautaro", "Loncoche", "Melipeuco", "Nueva Imperial", "Padre las Casas", "Perquenco", "Pitrufquén", "Pucón", "Saavedra", "Teodoro Schmidt", "Toltén", "Vilcún", "Villarrica", "Cholchol", "Angol", "Collipulli", "Curacautín", "Ercilla", "Lonquimay", "Los Sauces", "Lumaco", "Purén", "Renaico", "Traiguén", "Victoria"]
    },
    {
        "region": "Región de Los Ríos",
        "comunas": ["Valdivia", "Corral", "Lanco", "Los Lagos", "Máfil", "Mariquina", "Paillaco", "Panguipulli", "La Unión", "Futrono", "Lago Ranco", "Río Bueno"]
    },
    {
        "region": "Región de Los Lagos",
        "comunas": ["Puerto Montt", "Calbuco", "Cochamó", "Fresia", "Frutillar", "Los Muermos", "Llanquihue", "Maullín", "Puerto Varas", "Castro", "Ancud", "Chonchi", "Curaco de Vélez", "Dalcahue", "Puqueldón", "Queilén", "Quellón", "Quemchi", "Quinchao", "Osorno", "Puerto Octay", "Purranque", "Puyehue", "Río Negro", "San Juan de la Costa", "San Pablo", "Chaitén", "Futaleufú", "Hualaihué", "Palena"]
    },
    {
        "region": "Región Aisén del Gral. Carlos Ibáñez del Campo",
        "comunas": ["Coihaique", "Lago Verde", "Aisén", "Cisnes", "Guaitecas", "Cochrane", "O’Higgins", "Tortel", "Chile Chico", "Río Ibáñez"]
    },
    {
        "region": "Región de Magallanes y de la Antártica Chilena",
        "comunas": ["Punta Arenas", "Laguna Blanca", "Río Verde", "San Gregorio", "Cabo de Hornos (Ex Navarino)", "Antártica", "Porvenir", "Primavera", "Timaukel", "Natales", "Torres del Paine"]
    },
    {
        "region": "Región Metropolitana de Santiago",
        "comunas": ["Cerrillos", "Cerro Navia", "Conchalí", "El Bosque", "Estación Central", "Huechuraba", "Independencia", "La Cisterna", "La Florida", "La Granja", "La Pintana", "La Reina", "Las Condes", "Lo Barnechea", "Lo Espejo", "Lo Prado", "Macul", "Maipú", "Ñuñoa", "Pedro Aguirre Cerda", "Peñalolén", "Providencia", "Pudahuel", "Quilicura", "Quinta Normal", "Recoleta", "Renca", "San Joaquín", "San Miguel", "San Ramón", "Vitacura", "Puente Alto", "Pirque", "San José de Maipo", "Colina", "Lampa", "Tiltil", "San Bernardo", "Buin", "Calera de Tango", "Paine", "Melipilla", "Alhué", "Curacaví", "María Pinto", "San Pedro", "Talagante", "El Monte", "Isla de Maipo", "Padre Hurtado", "Peñaflor"]
    }]
}
/*Formulario y Validaciones*/
$(function () {
    $("#formulario1").validate({
        rules: {
            username: {
            required: true,
            minlength: 5
            },
            correo: {
                required: true,
                email: true
            },
            nombre: {
                required: true,
                minlength: 7
            },
            ntelefono: {
                required: true,
            },
            run: {
                required: true,
            },
            contrasenia: {
                required: true,
            },
            recontrasenia: {
                equalTo: "#contrasenia"
            }
        },
        messages: {
            username: {
                required: "Debe ingresar un nombre de usuario",
                minlength: "Debe tener un largo mínimo de 5 caracteres"
            },
            correo: {
                required: "Debe ingresar su correo",
                email: "Debe ingresar un correo de formato correcto"
            },
            nombre: {
                required: "Debe ingresar su nombre",
                minlength: "Debe ingresar al menos 7 caracteres"
            },
            fnacimiento: {
                required: "Debe ingresar su fecha de nacimiento",
                max: "La fecha no puede ser mayor al año 2001"
            },
            contrasenia: {
                required: "Debe ingresar su contraseña",
                minlength: "El largo minimo es de 6 caracteres"
            },
            recontrasenia: {
                required: "Debe volver a ingresar su contraseña",
                equalTo: "Debe volver a ingresar su contraseña"
            },
            ntelefono: {
                required: "Debe ingresar su número telefonico"
            },
            run: {
                required: "Debe ingresar su run"
            },
        }
    })
    
    $("#formulario1").submit(function () {
        if (Fn.validaRut($("#run").val())) {
            console.log("RUT CORRECTO")
        } else {
            console.log("RUT INCORRECTO")
        }
        return true;
    })
    /*ValidadorRUt*/
    var Fn = {
        validaRut: function (rutCompleto) {
            if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(rutCompleto))
                return false;
            var tmp = rutCompleto.split('-');
            var digv = tmp[1];
            var rut = tmp[0];
            if (digv == 'K') digv = 'k';
            return (Fn.dv(rut) == digv);
        },
        dv: function (T) {
            var M = 0, S = 1;
            for (; T; T = Math.floor(T / 10))
                S = (S + T % 10 * (9 - M++ % 6)) % 11;
            return S ? S - 1 : 'k';
        }
    }

})
/*validar numeros telefono*/
function validaNumericos(event) {
    if (event.charCode >= 48 && event.charCode <= 57) {
        return true;
    }
    return true;
}

/*Tipo de Casa */
var arreglo = []

$("#formulario1").submit(function () {
    var respuestas = $(this).serializeObject();
    /*Añadido al form*/
    arreglo.push(respuestas)
    $("#arreglo").html("")
    arreglo.forEach(elemento => {
        console.log(elemento)
        $("#arreglo").append
            ('<div class="col-md-2">' +
            '<img class="img-fluid" src="' + elemento.foto + '" alt="" srcset="">' +
            '<h6>' + elemento.correo + '</h6>' +
            '<p>' + elemento.casa + '</p>' +
            '</div>')
    });

    return true;
})

/* Validaciones Rescatados */
$(function () {
    $("#formulario2").validate({
        rules: {
            nombrep: {
                required: true,
                minlength: 3
            },
            raza : {
                required: true,
            },
            descripcion : {
                required: true,
                minlength: 10
            },
            estado : {
                required: true              
            }
        },
        messages : {
            nombrep: {
                required: "Debe ingresar un nombre al perro",
                minlength: "Debe tener un minimo de 3 caracteres"
            },
            raza: {
                required: "Debe ingresar una raza"
            },
            descripcion: {
                required: "Debe ingresar una descripcion para el perro",
                minlength: "La descripcion no puede ser de menos de 10 digitos"
            },
            estado: {
                required: "Debe seleccionar un estado para el perro"
            }
        }
    })
})

/*Validacion Login*/
$(function () {
    $("#login1").validate({
        rules: {
            username: {
                required: true,
            },
            contrasenia: {
                required: true
            },
        },
        messages : {
            nombrep: {
                required: "Debe ingresar un nombre de usuario"
                },
            raza: {
                required: "Debe ingresar su contraseña"
                }
            }        
        })
    })




$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
