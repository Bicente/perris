from django.urls import path, include
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('',views.index,name="index"),
    path('formulario/',views.formulario,name="formulario"),
    path('login/',views.login,name="login"),
    path('administrador/',views.administrador,name="administrador"),
    path('formulario/persona/',views.crear,name="crear"),
    path('administrador/rescatados',views.rescatado,name="rescatado"),
    path('user/lista',views.perro_list,name="listar"),
    path('eliminar/<int:id>',views.eliminar,name ='eliminar'),
    path('ver/<int:id>',views.buscarPerro,name ='buscarPerro'),
    path('editar/<int:id>',views.editar,name ='editar'),
    path('editado/<int:id>',views.editado,name ='editado'),
    #path('login',views.login,name="login"),
    path('cerrarsession',views.cerrar_session,name="cerrar_session"),
    path('login/iniciar',views.login_iniciar,name="iniciar"),
    url(r'^oauth/', include('social_django.urls', namespace='social')),
]