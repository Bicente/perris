from django.db import models

# Create your models here.

class Persona(models.Model):
    username = models.CharField(max_length= 100)
    contrasenia = models.CharField(max_length= 100)
    recontrasenia = models.CharField(max_length= 100)
    correo = models.CharField(max_length= 100)
    run = models.CharField(max_length= 15)
    nombre = models.CharField(max_length= 100)
    fnacimiento = models.DateField()
    ntelefono = models.IntegerField()
    region = models.CharField(max_length= 100)
    comuna = models.CharField(max_length= 100)
    casa = models.CharField(max_length= 100)

    def __str__(self):
        return "username: "+self.username+" contrasenia: "+self.contrasenia+" recontrasenia: "+self.recontrasenia+" correo: "+self.correo+" run: "+self.run+" nombre: "+self.nombre+" fnacimiento: "+self.fnacimiento+" ntelefono: "+self.ntelefono+" region: "+self.region+" comuna: "+self.comuna+" casa: "+self.casa

class Perro(models.Model):
    nombrep = models.CharField(max_length= 100)
    raza = models.CharField(max_length= 100)
    descripcion = models.CharField(max_length= 100)
    estado = models.CharField(max_length= 100)
    fotito = models.ImageField(upload_to='foto/')

    def __str__(self):
        return "nombrep: "+self.nombrep+" raza: "+self.raza+" descripcion: "+self.descripcion+" estado: "+self.estado