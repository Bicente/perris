from django.shortcuts import render
from django.http import HttpResponse
from .models import Persona
from .models import Perro
from django.contrib import messages
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group
from django.shortcuts import render
from rest_framework import viewsets, serializers
from Perris.serializers import PerroSerializer, PersonaSerializer
# Create your views here.

#importar user
from django.contrib.auth.models import User
#sistema de autenticación 
from django.contrib.auth import authenticate

#retorna a la pagina index
def index(request):
    return render(request,'index.html',{})

#retorna a la pagina de crud de perro
def perro_list(request):
   return render(request,'list.html',{'elementos':Perro.objects.all()})

#retorna a la pagina del registro de usuario
def formulario(request):
    return render(request,'formulario.html',{})

#retorna a la pagina de login (falta implementar)
def login(request):
    return render(request,'login.html',{})
    
#retorna a la pagina de registro de perro
def administrador(request):
    return render(request,'administrador.html',{})

#crea un objeto perro y lo guarda en la base de datos
def rescatado(request):
      nombrep = request.POST.get('nombrep','')
      raza = request.POST.get('raza','')
      descripcion = request.POST.get('descripcion','')
      estado = request.POST.get('estado','')
      foto = request.FILES.get('foto',False)
      perro = Perro(nombrep=nombrep,raza=raza,descripcion=descripcion,estado=estado,fotito=foto)
      perro.save()
      return redirect('administrador')
      

     
      
#crea un objeto persona(usuario) y lo guarda en la base de datos
def crear(request):
    username = request.POST.get('username','')
    contrasenia = request.POST.get('contrasenia','')
    recontrasenia = request.POST.get('recontrasenia','')
    correo = request.POST.get('correo','')
    run = request.POST.get('run','')
    nombre = request.POST.get('nombre','')
    fnacimiento = request.POST.get('fnacimiento','')
    ntelefono = request.POST.get('ntelefono','')
    region = request.POST.get('region','')
    comuna = request.POST.get('comuna','')
    casa = request.POST.get('casa','')

    persona = Persona(username=username,contrasenia=contrasenia,recontrasenia=recontrasenia,correo=correo,run=run,nombre=nombre,fnacimiento=fnacimiento,ntelefono=ntelefono,region=region,comuna=comuna,casa=casa)
    persona.save()
   
    return redirect('formulario')

#muestra el nombre del perro en una pagina responsiba
def buscarPerro(request,id):
    perro = Perro.objects.get(pk = id)
    return HttpResponse("encontrado" + perro.nombrep)      

#elimina aun perro

def eliminar(request,id):
    perro = Perro.objects.get (pk = id)
    perro.delete()
    return redirect('listar')

#retorna a la pagina de editado
def editar(request,id):
    perro = Perro.objects.get (pk = id)
    return render(request,'editar.html',{'perro' : perro} )

#rellena los campos vacios del editado con los datos del objeto y lo guarda y retorna a la pagina de lista
def editado(request,id):
    perro = Perro.objects.get (pk = id)
    nombrep = request.POST.get('nombrep','')
    raza = request.POST.get('raza','')
    descripcion = request.POST.get('descripcion','')
    estado = request.POST.get('estado','')
    foto = request.FILES.get('foto',False)
    perro.nombrep = nombrep
    perro.raza = raza
    perro.descripcion = descripcion
    perro.save()
    return redirect('listar')

#Inicio de sesion
def cerrar_session(request):
    del request.session['usuario']
    return redirect('index')

def login(request):
    return render(request,'login.html',{})

def login_iniciar(request):
    username = request.POST.get('nombre_usuario','')
    contrasenia = request.POST.get('contrasenia','')
    
    user = authenticate(username=username, contrasenia=contrasenia)

    if user is not None:
        request.session['usuario'] = user.first_name+" "+user.last_name
        return redirect("list")
    else:
        return redirect("index")

def home(request):
    return render(request, 'list.html')

class PersonaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Persona.objects.all().order_by('nombre')
    serializer_class = PersonaSerializer
 
class PerroViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Perro.objects.all()
    serializer_class = PerroSerializer